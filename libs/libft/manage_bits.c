/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_bits.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/23 22:41:22 by Diana             #+#    #+#             */
/*   Updated: 2021/05/12 20:24:03 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		set_bit(const int value, const int pos)
{
	return (value | (1 << pos));
}

int		get_bit(const int value, const int pos)
{
	return ((value & (1 << pos)) != 0);
}

int		unset_bit(const int value, const int pos)
{
	return (value & ~(1 << pos));
}

