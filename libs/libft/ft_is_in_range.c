/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_in_range.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Diana <Diana@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/05 03:21:51 by Diana             #+#    #+#             */
/*   Updated: 2020/10/09 18:31:27 by Diana            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

bool		ft_is_in_range(float value, float min, float max)
{
	if ((value > max) || (value < min))
		return (false);
	return (true);
}
