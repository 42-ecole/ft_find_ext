/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   funcs.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:06:18 by rfunk             #+#    #+#             */
/*   Updated: 2021/01/13 21:33:13 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

bool		compare(t_format *f, char *text_format)
{
	char i;

	i = 0;
	while (*(f->pos + i) && *text_format)
		if (*(f->pos + i++) != *(text_format++))
			return (0);
	f->pos += i - 1;
	return (1);
}

void		add_values(t_format *f, char value, int size)
{
	while (size-- > 0)
		add_char(f, value);
}
