/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ascii_manager.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:05:59 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/04 14:12:13 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

static void	manage_value(t_format *f, long value, char *str, size_t size)
{
	str[size] = '\0';
	while (size-- > 0)
	{
		if (f->keys & MINUS)
			str[size] = (value % 10 - 48) * -1;
		else
			str[size] = value % 10 + 48;
		value /= 10;
	}
}

static void	cieling(t_format *f, char *str, char value)
{
	int i;

	if (value < 5)
		return ;
	i = ft_strlen2(str);
	while (--i > -1)
	{
		if (str[i] == '.')
			continue ;
		else if (str[i] == 57)
			str[i] = 48;
		else if (str[i] < 57)
		{
			str[i]++;
			return ;
		}
	}
	f->keys |= PREFFIX;
}

static void	manage_fraction(t_format *f, long double val,
			char *str, size_t size)
{
	if (f->prec == 0)
		cieling(f, str, val * 10);
	else
	{
		str[size++] = '.';
		val = ABS(val);
		while (f->prec-- > 0)
		{
			val *= 10;
			str[size++] = (long)val % 10 + 48;
			val -= (long)val;
		}
		str[size] = '\0';
		cieling(f, str, val * 10);
	}
}

void		ld_to_ascii(t_format *f, long double value, size_t size, char *str)
{
	manage_value(f, (long)value, str, size);
	manage_fraction(f, value - (long long)value, str, size);
}
