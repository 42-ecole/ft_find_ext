/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:06:12 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/10 13:16:52 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	parse_digits(t_format *f)
{
	int value;

	if (*(f->pos) == 48)
	{
		f->keys |= ZERO;
		return ;
	}
	value = 0;
	while (DIGIT(*(f->pos)))
	{
		value = value * 10 + *(f->pos) - 48;
		f->pos++;
	}
	if (*(f->pos) == '$')
	{
		f->keys |= DOLL;
		f->argc = value;
	}
	else
	{
		f->keys |= WIDTH;
		f->w = value;
		f->pos--;
	}
}

static void	parse_precision(t_format *f)
{
	f->pos++;
	f->keys |= PREC;
	if (*(f->pos) == '*')
		f->prec = va_arg(f->lst, int);
	else
	{
		while (DIGIT(*(f->pos)))
		{
			f->prec = f->prec * 10 + *(f->pos) - 48;
			f->pos++;
		}
		f->pos--;
	}
	if (f->prec < 0)
	{
		f->prec = 0;
		f->keys ^= PREC;
	}
}

static void	parse_size(t_format *f)
{
	if (*(f->pos) == 'l' && *(f->pos + 1) != 'l')
		f->keys |= LONG;
	else if (*(f->pos) == 'l' && *(f->pos + 1) == 'l' && f->pos++)
		f->keys |= LONG1;
	else if (*(f->pos) == 'L')
		f->keys |= LONG2;
	else if (*(f->pos) == 'h' && *(f->pos + 1) != 'h')
		f->keys |= SHORT;
	else if (*(f->pos) == 'h' && *(f->pos + 1) == 'h' && f->pos++)
		f->keys |= SHORT1;
	else if (*(f->pos) == 'j')
		f->keys |= INTMAX;
	else if (*(f->pos) == 'z')
		f->keys |= SIZE_T;
}

static void	capability_check(t_format *f)
{
	if (f->keys & PLUS && f->keys & SPACE)
		f->keys ^= SPACE;
	if (f->keys & ALIGN && f->keys & ZERO)
		f->keys ^= ZERO;
	if (f->keys & PREC && f->keys & ZERO && f->prec != 0
		&& *(f->pos) != 'f' && *(f->pos) != 'F')
		f->keys ^= ZERO;
	if (f->w < 0)
	{
		f->w = ABS(f->w);
		f->keys |= ALIGN;
	}
	if ((*(f->pos) == 'f' || *(f->pos) == 'F') && !(f->keys & PREC))
		f->prec = 6;
}

void		printf_parse_flags(t_format *f)
{
	f->pos++;
	while (*(f->pos))
	{
		if (DIGIT(*(f->pos)))
			parse_digits(f);
		else if (*(f->pos) == ' ' || *(f->pos) == '+')
			f->keys |= (*(f->pos) == '+') ? PLUS : SPACE;
		else if (*(f->pos) == '-' || *(f->pos) == '#')
			f->keys |= (*(f->pos) == '-') ? ALIGN : HASH;
		else if (*(f->pos) == '.')
			parse_precision(f);
		else if (*(f->pos) == '*' && (f->keys |= WIDTH))
			f->w = va_arg(f->lst, int);
		else if (*(f->pos) == 'l' || *(f->pos) == 'L' || *(f->pos) == 'h')
			parse_size(f);
		else if (*(f->pos) == 'j' || *(f->pos) == 'z')
			parse_size(f);
		else
			break ;
		f->pos++;
	}
	capability_check(f);
}
