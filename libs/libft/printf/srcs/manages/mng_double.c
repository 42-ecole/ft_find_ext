/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_double.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:07:04 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/04 14:12:07 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

static void	get_size(long d, size_t *size)
{
	*size = (d == 0) ? 1 : 0;
	while (d != 0)
	{
		d /= 10;
		*size += 1;
	}
}

static void	get_double(t_format *f, long double *d)
{
	va_list copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	if (f->keys & LONG2)
		*d = va_arg((f->keys & DOLL) ? copy : f->lst, long double);
	else
		*d = va_arg((f->keys & DOLL) ? copy : f->lst, double);
}

static void	print_double(t_format *f, long double d, size_t size)
{
	char value[size + f->prec + 2];

	ld_to_ascii(f, d, size, value);
	size = ft_strlen2(value);
	if (f->keys & PLUS || f->keys & SPACE || f->keys & MINUS)
		size++;
	if (f->keys & PREFFIX)
		size++;
	if ((size_t)f->w > size && !(f->keys & ALIGN) && !(f->keys & ZERO))
		add_values(f, 32, (size_t)f->w - size);
	if (f->keys & PLUS || f->keys & SPACE || f->keys & MINUS)
		add_sign(f);
	if (f->keys & PREFFIX)
		add_char(f, 49);
	if ((size_t)f->w > size && !(f->keys & ALIGN) && f->keys & ZERO)
		add_values(f, 48, (size_t)f->w - size);
	add_string(f, value);
	if ((size_t)f->w > size && f->keys & ALIGN)
		add_values(f, 32, (size_t)f->w - size);
}

void		mng_double(t_format *f)
{
	size_t			size;
	long double		d;

	get_double(f, &d);
	if (d < 0)
		f->keys |= MINUS;
	get_size(d, &size);
	print_double(f, d, size);
}
