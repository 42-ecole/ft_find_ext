/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_unsigned.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:07:45 by rfunk             #+#    #+#             */
/*   Updated: 2021/01/13 21:36:29 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	get_unsigned(t_format *f, uintmax_t *un)
{
	va_list copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	if (f->keys & INTMAX || f->keys & LONG1)
		*un = va_arg((f->keys & DOLL) ? copy : f->lst, uintmax_t);
	else if (f->keys & SHORT)
		*un = (unsigned short)va_arg((f->keys & DOLL) ? copy : f->lst,
		unsigned int);
	else if (f->keys & SHORT1)
		*un = (unsigned char)va_arg((f->keys & DOLL) ? copy : f->lst,
		unsigned int);
	else if (f->keys & SIZE_T)
		*un = va_arg((f->keys & DOLL) ? copy : f->lst, size_t);
	else if (f->keys & LONG || *(f->pos) == 'U')
		*un = va_arg((f->keys & DOLL) ? copy : f->lst, unsigned long int);
	else
		*un = va_arg((f->keys & DOLL) ? copy : f->lst, unsigned int);
}

static void	get_size(t_format *f, uintmax_t un, int *size)
{
	*size = (un == 0) ? 1 : 0;
	while (un != 0)
	{
		un /= 10;
		*size += 1;
	}
	if (f->keys & PREC)
		*size = (f->prec > *size) ? f->prec : *size;
	if (f->keys & WIDTH && f->keys & ZERO)
		*size = (f->w > *size) ? f->w : *size;
}

static void	print_unsigned(t_format *f, uintmax_t un, int size)
{
	if (size > 0)
	{
		if (f->keys & PREC && f->prec == 0 && un == 0)
		{
			if (f->w > 0)
				add_char(f, 32);
			return ;
		}
		print_unsigned(f, un / 10, --size);
		add_char(f, un % 10 + 48);
	}
}

void		mng_unsigned(t_format *f)
{
	int			size;
	uintmax_t	un;

	get_unsigned(f, &un);
	get_size(f, un, &size);
	if (!(f->keys & ALIGN) && !(f->keys & ZERO))
		add_values(f, 32, f->w - size);
	print_unsigned(f, un, size);
	if (f->keys & ALIGN)
		add_values(f, 32, f->w - size);
}
