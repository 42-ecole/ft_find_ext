/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llst_enum.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/30 17:11:34 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/04 20:33:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "linked_list.h"

bool	llst_hasnext(void *sequence)
{
	t_llist *lst;

	lst = (t_llist *)sequence;
	if (!(lst->ptr->next))
		return (false);
	return (true);
}

bool	llst_movenext(void *sequence)
{
	t_llist *lst;

	lst = (t_llist *)sequence;
	if (!(lst->ptr->next))
		return (false);
	lst->ptr = lst->ptr->next;
	return (true);
}

void	llst_reset(void *sequence)
{
	t_llist  *lst;

	lst = (t_llist  *)sequence;
	lst->ptr = *(lst->head);
}

void	*llst_current(void *sequence)
{
	t_llist  *lst;

	lst = (t_llist  *)sequence;
	return (lst->ptr);
}

size_t	llst_get_content_size(void *sequence)
{
	t_llist *lst;

	lst = (t_llist *)sequence;
	return (lst->content_size);
}

size_t	llst_get_count(void *sequence)
{
	t_llist *lst;

	lst = (t_llist *)sequence;
	return (lst->count);
}
