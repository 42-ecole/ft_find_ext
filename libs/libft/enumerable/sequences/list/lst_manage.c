/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_manage.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/20 00:04:00 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 20:56:40 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list_p.h"
#include "list.h"
#include <stdio.h>

void		lst_add(struct s_list *lst, void *data)
{
	if (lst->count < lst->capacity)
	{
		ft_memcpy(lst->index_of(lst, lst->count), data, lst->content_size);
		lst->count++;
	}
}

void		lst_add_free(struct s_list *lst, void *data)
{
	if (lst->count < lst->capacity)
	{
		ft_memcpy(lst->index_of(lst, lst->count), data, lst->content_size);
		free(data);
		lst->count++;
	}
}

void		lst_add_size(struct s_list *lst, void *data, size_t content_size)
{
	if (lst->count < lst->capacity)
	{
		ft_memcpy(lst->index_of(lst, lst->count), data, content_size);
		if (content_size != lst->content_size)
			lst->count += content_size / lst->content_size;
		else
			lst->count++;
	}
}

void		*lst_remove(t_list *lst, int index)
{
	int		tmp;

	tmp = index;
	tmp++;
	
	lst->count--;
	return (NULL);
}

void		*lst_index_of(struct s_list *lst, int index)
{
	return (lst->objs + index * lst->content_size);
}

// rework with IComparator and type sensitive
bool		lst_is_descorder(t_list *lst)
{
	int		num;
	int		num2;
	int		i;

	i = 0;
	while (i + 1 < (int)lst->count)
	{
		num = *CAST_TYPE(int, lst->index_of(lst, i));
		num2 = *CAST_TYPE(int, lst->index_of(lst, i + 1));
		if (num < num2)
			return (false);
		i++;
	}
	return (true);
}

bool		lst_is_ascorder(t_list *lst)
{
	int		num;
	int		num2;
	int		i;

	i = 0;
	while (i + 1 < (int)lst->count)
	{
		num = *CAST_TYPE(int, lst->index_of(lst, i));
		num2 = *CAST_TYPE(int, lst->index_of(lst, i + 1));
		if (num > num2)
			return (false);
		i++;
	}
	return (true);
}


//probably rework with stack variable - tmp
void		lst_reverse(t_list *lst, int start, int end)
{
	void	*obj;
	void	*obj2;
	void	*tmp;

	tmp = ft_memalloc(lst->content_size);
	while (start < end)
	{

		obj = lst->index_of(lst, lst->count - start - 1);
		obj2 = lst->index_of(lst, lst->count - end - 1);
		ft_memcpy(tmp, obj, lst->content_size);
		ft_memcpy(obj, obj2, lst->content_size);
		ft_memcpy(obj2, tmp, lst->content_size);
		start++;
		end--;
	}
	free(tmp);
}

void	lst_shift(t_list *lst, int offset)
{
	if (offset < 0)
	{
		offset *= -1;
		offset = offset % lst->count;
		offset = lst->count - offset;
	}
	else
		offset = offset % lst->count;
	lst->reverse(lst, 0, lst->count - 1);
	lst->reverse(lst, 0, offset - 1);
	lst->reverse(lst, offset, lst->count - 1);
}

void	lst_addrng(t_list *self, t_list *lst)
{
	t_list	*buf;

	if (self->count + lst->count > self->capacity)
	{
		//todo with realloc
		buf = new_list((self->count + lst->count) * 2, self->content_size);
		buf->add_range(buf, self);
		buf->add_range(buf, lst);
	}
	else
	{
		while (true)
		{
			self->add(self, lst->enumerable.current(lst));
			if (!lst->enumerable.move_next(lst))
				break;
		}
		self->enumerable.reset(self);
	}
}

t_list		*new_sublist(t_list *src, int index, int len)
{
	t_list	*lst;

	if (index >= (int)src->count)
	{
		lst = new_list(2 * len, src->content_size);
		return (lst);
	}
	lst = new_list(2 * len, src->content_size);
	src->enumerable.reset(src);
	while (index-- && src->enumerable.move_next(src));
	while (len)
	{
		lst->add(lst, src->enumerable.current(src));
		if (!src->enumerable.move_next(src))
			break;
		len--;
	}
	src->enumerable.reset(src);
	lst->enumerable.reset(lst);
	return (lst);
}