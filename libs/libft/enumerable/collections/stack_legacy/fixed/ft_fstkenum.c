/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fstkenum.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/30 18:58:20 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/14 16:49:50 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

bool	fstk_movenext(void *enumerable)
{
	t_istack *stack;

	stack = (t_istack *)enumerable;
	if (stack->ptr < &(stack->objs[stack->count - 1]))
	{
		stack->ptr = (stack->ptr + 1);
		return (true);
	}
	return (false);
}

void	fstk_reset(void *enumerable)
{
	t_istack *stack;

	stack = (t_istack *)enumerable;
	stack->ptr = stack->objs;
}

void	*fstk_current(void *enumerable)
{
	t_istack *stack;

	stack = (t_istack *)enumerable;
	return (stack->ptr);
}

void		fstk_link_enumerable(struct s_ienumerable *enumerable)
{
	enumerable->current = fstk_current;
	enumerable->move_next = fstk_movenext;
	enumerable->reset = fstk_reset;
}