// /* ************************************************************************** */
// /*                                                                            */
// /*                                                        :::      ::::::::   */
// /*   ft_fstknew.c                                       :+:      :+:    :+:   */
// /*                                                    +:+ +:+         +:+     */
// /*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
// /*                                                +#+#+#+#+#+   +#+           */
// /*   Created: 2020/10/28 16:42:40 by rfunk             #+#    #+#             */
// /*   Updated: 2020/10/31 00:35:49 by rfunk            ###   ########.fr       */
// /*                                                                            */
// /* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

t_istack	*ft_newfstk(size_t max_size, size_t content_size)
{
	t_istack	*stack;

	stack = ft_memalloc(sizeof(t_istack));
	stack->objs = ft_memalloc(sizeof(t_object) * max_size);
	ft_bzero(stack->objs, content_size * max_size);
	stack->content_size = content_size;
	stack->count = 0;
	stack->max_size = max_size;
	fstk_link_enumerable(&(stack->enumerable));
	stack->push = fstk_push;
	stack->pop = fstk_pop;
	stack->pop_obj = fstk_popobj;
	stack->peek = fstk_peek;
	stack->index_of = fstk_indexof;
	stack->reverse = fstk_reverse;
	stack->shift = fstk_shift;
	stack->is_sorted = ftsk_is_ascorder;
	return (stack);
}
