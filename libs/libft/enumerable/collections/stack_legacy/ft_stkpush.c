/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stkpush.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 18:22:35 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/04 16:58:55 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

void		stk_pushobj(struct s_istack *stack, t_object *obj)
{
	t_object	*new;

	stack->count++;
	new = (t_object *)obj;
	if (stack->ptr)
	{
		new->next = stack->ptr;
		stack->ptr = new;
		stack->objs = stack->ptr;
	}
	else
	{
		stack->objs = new;
		stack->enumerable.reset(stack);
	}
}

void		stk_push(struct s_istack *stack, void *obj)
{
	t_object	*new;

	new = new_object(obj, stack->content_size);
	stk_pushobj(stack, new);
}
