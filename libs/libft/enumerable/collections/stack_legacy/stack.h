/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 17:21:30 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/08 16:43:09 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_H
# define STACK_H

# include <stdbool.h>
# include <string.h>
# include <stdlib.h>
# include "../memory/memory.h"
# include "../enumerable/enumerable.h"


typedef struct	s_istack
{
	char					*type;
	t_object				*objs;
	t_object				*ptr;
	size_t					count;
	size_t					max_size;
	size_t					content_size;
	struct s_ienumerable	enumerable;
	void					(*link_enumerable)(struct s_ienumerable);
	void					(*push)(struct s_istack *, void *);
	void					(*push_obj)(struct s_istack *, t_object *);
	void					*(*pop)(struct s_istack *);
	t_object				*(*pop_obj)(struct s_istack *);
	void					*(*peek)(struct s_istack *);
	void					*(*index_of)(struct s_istack *, int);
	void					(*reverse)(struct s_istack *, int, int);
	void					(*shift)(struct s_istack *, int);
	bool					(*is_sorted)(struct s_istack *);
}				t_istack;

/*
* Dynamic stack, which represented as linked list.
*/
t_istack	*ft_newstk(size_t content_size);

void		stk_link_enumerable(struct s_ienumerable *enumerable);

/*
* Fixed stack, which contains array of objects.
* Use if size of array is specified.
*/
t_istack	*ft_newfstk(size_t max_size, size_t content_size);

void		fstk_link_enumerable(struct s_ienumerable *enumerable);

#endif