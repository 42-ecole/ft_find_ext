/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hasht_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/24 17:13:08 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/06 15:59:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash_table.h"
#include "hash_table_p.h"

t_hash_table	*new_hash_table(size_t capacity,
				size_t content_size, unsigned int (*hash_func)(void *))
{
	t_hash_table	*table;

	table = ft_memalloc(sizeof(t_hash_table));
	table->seq = new_list(capacity, sizeof(t_llist));
	table->count = capacity;
	table->content_size = content_size;
	table->hash = hash_func;
	table->add = hasht_add;
	table->search = hash_search;
	// table->remove = hasht_remove;
	return (table);
}
