/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_table_p.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/24 17:12:48 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/06 15:59:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HASH_TABLE_P_H
# define HASH_TABLE_P_H

#include "hash_table.h"

void			hasht_add(t_hash_table *table, void *data);
void			*hash_search(t_hash_table *table, void *data);
void			hasht_remove(t_hash_table *table, void *data);

#endif