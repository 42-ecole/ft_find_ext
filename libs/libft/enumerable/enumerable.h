#ifndef ENUMERABLE_H
# define ENUMERABLE_H

# include <stdbool.h>
# include "i_object.h"

/*
* The interface describes the behavior of enumerated abstracts. 
* You are MUST define and implement these features.
* \param move_next moves pointer to the next element.
* \param current return object pointed to by the pointer.
* \param reset resets pointer to the begining.
*/
struct	s_ienumerable
{
	bool					(*move_next)(void *);
	bool					(*has_next)(void *);
	void					*(*current)(void *);
	void					(*reset)(void *);
	size_t					(*get_content_size)(void *);
	size_t					(*get_count)(void *);
};

/*
* For each element in enumerable run function with func_data as argument.
* Use NULL in func_data if you haven't argument for the function.
* \param ienumerable enumeration interface that is implemented in the sequence.
* \param sequence void pointer to sequence.
* \param func function that is used to execute code within an enumeration.
* \param func_data argument for executable function.
*/
void		foreach(struct s_ienumerable ienumerable, void *sequence,
				void (*func)(void *, void *), void *func_data);

void	add_range(struct s_ienumerable idst, void *dst,
	struct s_ienumerable isrc, void *src,
	int low, int len);

#endif
