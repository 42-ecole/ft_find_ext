NAME := ft_findext
 
CC := gcc
CC_DEBUG := gcc -g
FLAGS = -Wall -Wextra
DEL := rm -rf
SRCS_PATH := ./srcs

#-----------SOURCES---------------#
SRC := main.c

SRC_DIR := $(addprefix $(SRCS_PATH)/, $(SRC))

OBJ := $(SRC_DIR:.c=.o)
#----------------------------------#


#----------LIBRARIES--------------#
LIBFTDIR := libs/libft
LIBFTINC := .
LIBFT := ft
FTMAKE := +make -sC $(LIBFTDIR)/$(LIBFTINC)

LIBS := -L $(LIBFTDIR) -l $(LIBFT)

INCLUDES := -I $(SRCS_PATH) \
			-I $(LIBFTDIR)/$(LIBFTINC) \
			-I $(LIBLOGDIR)/$(LIBLOGINC)
#----------------------------------#


#-----------STYLES-----------------#
NONE = \033[0m
INVERT := \033[7m
GREEN := \033[32m
RED := \033[31m
SUCCESS := [$(GREEN)✓$(NONE)]
SUCCESS2 := [$(INVERT)$(GREEN)✓$(NONE)]
APPOK := $(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):$(NAME) has been successfully compiled.$(NONE)
APPDELETED := $(INVERT)$(RED)Removed ✓$(NONE)$(INVERT):$(NAME) has been successfully removed.$(NONE)
#----------------------------------#

all:
	@make -s -j4 $(NAME);

$(OBJ): %.o: %.c
	@echo -n $(NAME):' $@: '
	@$(CC) -c $(FLAGS) $(INCLUDES) $< -o $@
	@echo "$(SUCCESS)"

$(LIBFT):
	@$(FTMAKE)

$(LIBLOG):
	@$(LOGMAKE)

$(NAME):  $(OBJ) $(LIBFT)
	@$(CC) $(OBJ) $(LIBS) -o $(NAME)
	@echo "$(APPOK)"

debug:
	$(CC_DEBUG) $(SRC_DIR) $(LIBS) $(INCLUDES) -g -o $(NAME)

$(CC_DEBUG):
	@$(eval CC=$(CC_DEBUG))
	debug_all: $(CC_DEBUG) pre
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(NONE)"
	debug: $(CC_DEBUG) all
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(NONE)"

clean:
	@$(DEL) $(OBJ)
	@$(FTMAKE) clean

fclean: clean
	@$(FTMAKE) fclean
	@$(DEL) $(NAME)
	@echo "$(APPDELETED)"

norm:
	@norminette $(SRC_DIR)
	@$(FTMAKE) norm

re: fclean all

.PHONY: all fclean clean re pre debug
