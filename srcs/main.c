# include <stdio.h>
# include "libft.h"

void		add_bytes(t_list *bytes, char *str)
{
	int		len;
	size_t	i;

	i = 0;
	len = ft_strlen(str);
	while (i < ft_strlen(str))
	{
		unsigned char c = str[i];
		bytes->add(bytes, &c);
		++i;
	}
}

bool			is_directory(char *str)
{
	if (str[0] == 'd')
		return (true);
	return (false);	
}

char			*parse_ext(char *str)
{
	int			len;
	int			orig_len;
	char		*ext = NULL;

	len = orig_len = ft_strlen(str);
	--orig_len;
	while (--len > - 1)
	{
		if (str[len] == '.')
		{
			if (orig_len - len && orig_len > 2)
			{
				ext = ft_strnew(orig_len - len + 1);
				ft_strcpy(ext, str + len + 1);
				break;
			}
		}
	}
	return (ext);
}

bool		is_contains(void *obj, void *data)
{
	t_object *obj1;
	t_object *obj2;

	obj1 = (t_object *)obj;
	obj2 = (t_object *)data;
	if (!obj1 || !obj2)
		return (true);
	if (!ft_strcmp((char *)obj1->content, (char *)obj2->content))
		return (true);
	return (false);
}

void		try_add(t_llist **ext, char *str)
{
	t_object	*obj;

	if (!(*ext)->count)
	{
		(*ext)->add_obj(*ext,
					new_object(str, ft_strlen(str)));
		return ;
	}

	obj = new_object(str, ft_strlen(str));
	t_list *tmp = ft_select((*ext)->enumerable, *ext,
						is_contains, obj);
	if (tmp && !tmp->count)
		(*ext)->add_obj(*ext, obj);
	free(tmp);
}

int			read_data(t_llist **ext_dic, int fd)
{
	char		*line;
	char		**columns;
	int			col_count;
	char		*ext;

	while ((get_next_line(fd, &line)) > 0)
	{
		columns = ft_strsplit_withcount(line, ' ', &col_count);
		if (col_count < 8 || is_directory(columns[0]))
			continue;
		if (!columns)
			exit(1);
		ext = parse_ext(columns[col_count - 1]);
		if (ext)
			try_add(ext_dic, ext);
		free(line);
		// free(ext);
	}
	return (0);
}

void	print_el(void *obj, void *data)
{
	t_object *t_obj = (t_object *)obj;
	ft_printf("%s\n", (char *)t_obj->content);
}

int		main(void)
{
	t_llist		*ext;

	ext = new_llist(1);
	read_data(&ext, 0);
	foreach(ext->enumerable, ext, print_el, NULL);
}